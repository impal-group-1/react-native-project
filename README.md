# Cinemakuy React-Native Apps
to Build
###### Clone or Pull
```
$ git clone https://gitlab.com/impal-group-1/react-native-project.git
or
$ git init
$ git remote add origin https://gitlab.com/impal-group-1/react-native-project.git
$ git pull origin master
```
###### Install (make sure that you install node.js already)
```
npm install
```
###### Install React-Native Cli(make sure that you install node.js already)
```
npm install -g react-native-cli
```