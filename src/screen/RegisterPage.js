import React, { Component, Fragment } from 'react';
import { Alert, Text, View,ImageBackground, Image,StatusBar} from 'react-native';
import { Button, Input, Icon} from 'react-native-elements';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Images from '../library/images';
import CinemakuyAPI from '../library/CinemakuyAPI';
import Style from './style/RegisterPageStyle';
import AsyncStorage from '@react-native-community/async-storage';
import {ToastAndroid} from 'react-native';
import InputForm from './subscreen/register/InputForm';


class RegisterPage extends Component {
	constructor(){
		super();
		this.RegisterProcessed = this.RegisterProcessed.bind(this);
	}
	static navigationOptions = {
		header: null ,
	}
	
	state = {
		loading:false,
		pass:"",
		phone:"",
		username:"",
		email:"",
		cpass:"",
		name:""
    }
	
	Validate(val){
		
		if(val.name == ""){
			return "Name Field is empty"
		}else if(val.username == ""){
			return "Username Field is empty"
		}else if(val.email == ""){
			return "E-mail Field is empty"
		}else if(val.phone == ""){
			return "Phone Field is empty"
		}else if(val.pass == ""){
			return "Password Field is empty"
		}else if(val.cpass == ""){
			return "Confirm password Field is empty"
		}else if(val.username.length < 6 || val.username == null){
			return "Username must have at least 6 words"
		}else if(val.pass.length < 6 || val.pass.length==null){
			return "Password must have at least 6 words"
		}else if(!val.email.includes("@")){
			return "Invalid Email Address"
		}else if(!(val.phone.substring(0, 2) == "08") && !(val.phone.substring(0, 1) == "+62") ){
			return "Invalid Phone number"
		}else if(val.pass != val.cpass){
			return "Password and Confirm password doesn't match"
		}
		
		return ""
	}
	
	CinemakuyRegisterProcess(){
		
		let validateResult = this.Validate(this.state);
		if(validateResult != ""){
			Alert.alert("Invalid Input" , validateResult);
			return
		}
		this.setState({loading:true});
		
		let request = {name : this.state.name ,username : this.state.username, email : this.state.email , 
						pass : this.state.pass ,phone :this.state.phone}

		console.log(request);
		
		CinemakuyAPI.Register(request , this.RegisterProcessed );
		
	}
	
	RegisterProcessed(response){
		console.log(response);
		if(response.isSuccess){
			this.props.navigation.navigate('Login');
			ToastAndroid.showWithGravity(
			  'Registration success',
			  ToastAndroid.SHORT,
			  ToastAndroid.BOTTOM,
				25,
				50,
			);
		}else{
			this.props.navigation.navigate('Login');
			ToastAndroid.showWithGravity(
			  'Registeration Failed, '+response.respon,
			  ToastAndroid.SHORT,
			  ToastAndroid.BOTTOM,
				25,
				50,
			  

			);
		}
	}
	
	render() {
		return (
			
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
				<StatusBar
					backgroundColor='#313C4C'
				/>
				<View style={Style.styleSheets.formContainer} >
					<ImageBackground
					  source={Images.registerBg}
					  style={Style.styleSheets.fullScreen}
					/> 
				</View>
				<View style={Style.styleSheets.formContainer} >
				
					<View style={{width:'100%',alignItems: 'center',}}>
					
					<Text style={{color:'#ffffff',fontFamily:Style.main.fonts.fontElegance,fontSize:30,marginTop:20,letterSpacing:5}}>REGISTER</Text>
					<Image
					  source={Images.logo}
					  style={{ width: Style.main.screenWidth/3, height: Style.main.screenWidth/3, marginTop:Style.main.screenHeight/64 , marginBottom:Style.main.screenHeight/30}}
					/>
					
					<Input
						  placeholder='Name'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({name:value}) }}
						  						  
						  leftIcon={
							<Icon
							  name='person'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					<Input
						  placeholder='Username'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({username:value}) }}
						  						  
						  leftIcon={
							<Icon
							  name='person-pin'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					<Input
						  placeholder='E-mail'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({email:value}) }}
						  						  
						  leftIcon={
							<Icon
							  name='mail'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					<Input
						  placeholder='Phone'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({phone:value}) }}
						  						  
						  leftIcon={
							<Icon
							  name='call'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					
					<Input
						  placeholder='Password'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({pass:value}) }}
						  secureTextEntry={true} 
						  						  
						  leftIcon={
							<Icon
							  name='lock'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					
					<Input
						  placeholder='Confirm Password'
						  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
						  containerStyle={{ width:'68%',marginBottom:15}}
						  inputContainerStyle={{ borderBottomColor: '#FD6C57' }}
						  placeholderTextColor='#FD6C57'
						  onChangeText={(value) => { this.setState({cpass:value}) }}
						  secureTextEntry={true} 
						  						  
						  leftIcon={
							<Icon
							  name='lock'
							  size={20}
							  color='#FD6C57'
							  style={{marginLeft:-10}}
							/>
						  }
						/>
					
					
			
					
					<Button 
					  buttonStyle={Style.styleSheets.loginButton}
					  onPress={() => {this.CinemakuyRegisterProcess();}}
					  loading={this.state.loading}
					  title="REGISTER" titleStyle={{color:'#FD6C57',fontFamily:Style.main.fonts.fontElegance}}
					  disabled={this.state.loading}
					></Button>
					
					<Text style={{color:'#ffffff',fontFamily:Style.main.fonts.fontElegance,fontSize:10,margin:20,marginBottom:0}}>
					Already member? 
					<Text> </Text>
					<Text style={{color:'#FD6C57',fontFamily:Style.main.fonts.fontElegance,fontSize:10,margin:20,marginBottom:0,textDecorationLine: 'underline'}}
					
					onPress={()=>{ 
						console.log("Login Page");
						this.props.navigation.navigate('Login');
					}}
					>
					Login Here!
					</Text></Text>
				</ View>
					
			</View>
		</View>
			
		);
	}
}

export default RegisterPage