import React, { Component } from 'react';
import { Button, Input,Icon } from 'react-native-elements';


class InputForm extends Component {
	constructor(){
		super();
		
		this.onTextChange= this.onTextChange.bind(this);
	}

	onTextChange(val){
		this.props.reff(val);
	}
	
	render() {
		return (
			
				<Input
					  placeholder={this.props.placeholder}
					  inputStyle={{color:'#ffffff',marginLeft:15,fontSize:12,paddingBottom:-10}}
					  containerStyle={{width:'68%',marginBottom:15,marginTop:-15}}
					  inputContainerStyle={{ borderBottomColor: '#A77076'}}
					  placeholderTextColor='#A77076'
					  onSubmitEditing={this.props.onSubmitEditing}
					  onChangeText={this.onTextChange}
					  leftIcon={
						<Icon
							name={this.props.icon}
							size={20}
							color='#A77076'
							style={{marginLeft:-10}}
						/>
					  }
					/>
			
		);
	}
}

export default InputForm