import {StyleSheet} from 'react-native'
import ApplicationStyles from './MainStyle'

export default { 
	main : ApplicationStyles,
	styleSheets : StyleSheet.create({
		fullScreen : {width: '100%', height: '101%'},
		formContainer : {
						position:'absolute',
						top:'0%',
						flex:1,
						width:'100%',
						height:'100%',
						alignItems: 'center',
						justifyContent: 'center'
		},
		loginButton : {
			borderRadius:100,
			marginTop:10,
			backgroundColor:'#FFFFFF',
			width:170,
			height:65
		}

	})
}