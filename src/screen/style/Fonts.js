const fonts = {
  fontElegance: 'RoundedElegance',
  fontAntDesign: 'AntDesign',
  fontEntypo: 'Entypo',
  fontEvillcons: 'Evillcons',
  fontFeather: 'Feather',
  fontFontAwesome: 'FontAwesome',
  fontFontAwesomeBrands: 'FontAwesome5_Brands',
  fontFontAwesomeRegular: 'FontAwesome5_Regular',
  fontFontAwesomeSolid: 'FontAwesome5_Solid',
  fontFontisto: 'Fontisto',
  fontFoundation: 'Foundation',
  fontIonicons: 'Ionicons',
  fontMaterialCommunityIcons: 'MaterialCommutityIcons',
  fontOcticons: 'Octicons',
  fontSimpleLineIcons: 'SimpleLineIcons',
  fontZocial: 'Zocial',
  fontOpenSans: 'OpenSans',
}

export default fonts
