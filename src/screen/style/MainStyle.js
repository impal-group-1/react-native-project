import Fonts from './Fonts'
import Colors from './Colors'
import { Dimensions } from 'react-native';

const ApplicationStyles = {
	fonts : Fonts,
	colors : Colors,
	screenWidth : Math.round(Dimensions.get('window').width),
	screenHeight : Math.round(Dimensions.get('window').height)
}

export default ApplicationStyles