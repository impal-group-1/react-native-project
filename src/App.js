import React, { Component } from 'react';
import { Text, View,Dimensions,Button} from 'react-native';
import LoginPage from './screen/LoginPage';
import RegisterPage from './screen/RegisterPage';
import HelloWorldApp2 from './screen/HelloWorldApp2';
import DetailPage from './screen/DetailPage';
import HomePage from './screen/HomePage';
import PickSeat from './screen/PickSeat';
import Checkout from './screen/Checkout';
import EditProfilePage from './screen/EditProfilePage';
import ChangePassPage from './screen/ChangePassPage';
import Tickets from './screen/Tickets';
import TopUp from './screen/TopUp';
import TopUpCheckout from './screen/TopUpCheckout';
import PaymentOrder from './screen/PaymentOrder';
import PaymentOrderDetails from './screen/PaymentOrderDetails';
import SideBar from './screen/subscreen/sidemenu/SideMenu';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

class HelloWorld extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Hello, world!!</Text>
      </View>
    );
  }
}


const MainNavigator = createStackNavigator({
  Login: {screen: LoginPage},
  Profile: {screen: HelloWorldApp2},
  Register: {screen: RegisterPage},
  Home: {screen: HomePage},
  EditProfile: {screen: EditProfilePage},
  ChangePass: {screen: ChangePassPage},
  Tickets:{screen : Tickets}, 
  Detail: {screen: DetailPage},
  PickSeat:{screen:PickSeat},
  Checkout:{screen:Checkout},
  TopUp:{screen:TopUp},
  TopUpCheckout:{screen:TopUpCheckout},
  PaymentOrder:{screen:PaymentOrder},
  PaymentOrderDetails : {screen:PaymentOrderDetails}
});

const App = createAppContainer(MainNavigator);

export default App;
