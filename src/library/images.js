const images = {
	loginBg: require('../../assets/background/log_bg.png'),
	registerBg: require('../../assets/background/regis_bg.png'),
	profileBg: require('../../assets/background/bg_P.png'),
	logo: require('../../assets/image/logo.jpg'),
	setting : require(('../../assets/image/setting.png')),
	tiket : require(('../../assets/image/tiket.png')),
	payment : require(('../../assets/image/payment.png')),
	set : require(('../../assets/image/set.png')),
	his : require(('../../assets/image/History.png')),	
	about : require(('../../assets/image/about.png')),	
	layerFade: require('../../assets/background/layer_homepage.png'),
}

export default images
